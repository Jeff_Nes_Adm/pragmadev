public class IMCInfo {
    
    String condition;
    Decimal imc;
    
    //Constructors
        
    public IMCInfo(String condition, Decimal imc){
        this.condition = condition;
        this.Imc = imc;
    }
        
    //O método principal recebe os valores de entrada do calculo e envia para os métodos auxiliares 
    public static IMCInfo checkIMC(Decimal weight, Decimal height, String gender){
        Decimal imc = calcIMC(weight,height);
        String condition = getCondition(imc,gender);            
         //por se tratar de um tipo primitivo complexto no return, passar como new   
        return new IMCInfo(condition,imc);
    }
    
    public static Decimal calcIMC(Decimal weight, Decimal height){
        Decimal imc = (weight/(height*height)*100)*100;
        return imc;
    }
    
    //utilizado query como String para não precisar estanciar uma lista.
    public static String getCondition(Decimal imc, String gender){
        BMITable__c bmiList = [SELECT Condition__c
                    	   	   FROM BMITable__c
                    	       WHERE (gender__c =: gender AND MinBMI__c <: imc AND MaxBMI__C >: imc )];
                                                              
        String condition = bmiList.Condition__c;
        
        //seria possivel no retorno passar desta forma 'return bmiList.Condition__c'
        //apenas quando souber que apenas um campo retornará no select
        return condition;
    }
       
   
}