public class ContactInfo {
    
    public String Name {get;set;}
    public String Email {
        get{return Email;}
        set{Email = value;}
    }
    
    public ContactInfo(){
        this.Name = '';
        this.Email = '';
    }
    
    public ContactInfo(String name, String email){
        this.Name = name;
        this.Email = email;
    }
    
	public static ContactInfo createContact(String name, String userName, String Service) {
   	 	String email = String.format('{0}@{1}.com',new List<Object>{userName, service});	
    	System.debug('email =>' + email);
        
    	return new ContactInfo(name,email);
	}
}